/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_options.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 16:20:52 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 16:21:37 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/ft_printf.h"
#include "ft_ls.h"

static int		ft_verify_options(t_opt *opt, char c)
{
	if (c == 'a')
		(*opt).opt_a = 1;
	else if (c == 'l')
	{
		if ((*opt).opt_one != 0)
			(*opt).opt_one = 0;
		(*opt).opt_l = 1;
	}
	else if (c == 'r')
		(*opt).opt_r = 1;
	else if (c == 't')
		(*opt).opt_t = 1;
	else if (c == 'R')
		(*opt).opt_br = 1;
	else if (c == '1')
	{
		if ((*opt).opt_l != 1)
			(*opt).opt_l = 0;
		(*opt).opt_one = 1;
	}
	else
		return (ERROR);
	return (0);
}

static int		ft_store_options(t_opt *opt, char *argv)
{
	int	i;

	i = 0;
	if (argv[i] != '-')
		return (FALSE);
	while (argv[++i])
	{
		if (ft_verify_options(opt, argv[i]) == ERROR)
		{
			(*opt).invalid = argv[i];
			return (ERROR);
		}
	}
	return (TRUE);
}

int				ft_parse_options(t_opt *opt, int argc, char **argv)
{
	int	i;
	int	ret;

	i = 1;
	ret = TRUE;
	while (i < argc && argv[i] && ret != FALSE && ret != ERROR)
	{
		ret = ft_store_options(opt, argv[i]);
		i++;
	}
	if (ret == ERROR)
		ft_opt_error((*opt).invalid);
	return (i - 1);
}
