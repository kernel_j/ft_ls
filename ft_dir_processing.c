/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dir_processing.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 20:15:03 by jwong             #+#    #+#             */
/*   Updated: 2016/05/25 14:16:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include "libft/includes/ft_printf.h"
#include "libft/includes/libft.h"
#include "ft_ls.h"

static void	ft_dir_read(DIR *dirp, t_data **db, char *argv, char l)
{
	t_data			*node;
	struct dirent	*dp;
	struct stat		sb;
	char			*path;

	while ((dp = readdir(dirp)) != NULL)
	{
		path = ft_path(argv, (*dp).d_name);
		if (lstat(path, &sb) == -1)
			ft_lstat_error();
		free(path);
		node = ft_new_node();
		ft_get_info(node, &sb);
		if ((*node).permissions[0] == 'l' && l == 1)
			ft_get_link(node, dp->d_name, argv);
		else
			(*node).path = ft_strdup((*dp).d_name);
		ft_add_node(db, node);
	}
}

int			ft_build_dir_db(t_data **db, char *argv, char l)
{
	DIR				*dirp;
	struct stat		sb;

	dirp = opendir(argv);
	if (dirp == NULL)
	{
		if (lstat(argv, &sb) != -1)
			if ((sb.st_mode & S_IFMT) == S_IFDIR)
				ft_printf("ft_ls: %s: %s\n", argv, strerror(errno));
		return (1);
	}
	else
	{
		if (l != 0 && ft_is_link(argv) == 0)
		{
			closedir(dirp);
			return (0);
		}
		ft_dir_read(dirp, db, argv, l);
	}
	closedir(dirp);
	return (0);
}

int			ft_process_options(t_opt *opt, char *argv)
{
	t_data	*db;
	char	l;

	l = 0;
	db = NULL;
	if ((*opt).opt_l != 0)
		l = 1;
	if (ft_build_dir_db(&db, argv, l) == 1)
		return (1);
	if ((*opt).opt_r != 0 && (*opt).opt_t != 0)
		ft_merge_sort_db(&db, ft_compare, 1);
	else if ((*opt).opt_r == 0 && (*opt).opt_t != 0)
		ft_merge_sort_db(&db, ft_compare, -1);
	else if ((*opt).opt_r != 0)
		ft_merge_sort_db(&db, ft_compare2, -1);
	else
		ft_merge_sort_db(&db, ft_compare2, 1);
	ft_print_db(db, opt, argv);
	if ((*opt).opt_br != 0)
		ft_recurse(db, opt, argv);
	ft_clean_lst(&db);
	return (0);
}

int			ft_dir_processing(t_data *arg, t_opt *opt, int i)
{
	t_data	*tmp;
	int		ret;

	tmp = arg;
	if ((*opt).opt_r != 0)
		ft_merge_sort_db(&tmp, ft_compare, 1);
	while (tmp)
	{
		(*opt).new_arg = 1;
		if (ft_process_options(opt, (*tmp).path) == 1)
			ret = 1;
		tmp = (*tmp).next;
		i++;
	}
	if (ret == 1)
		return (1);
	return (0);
}
