/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recurse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 20:01:29 by jwong             #+#    #+#             */
/*   Updated: 2016/05/25 14:36:48 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/ft_printf.h"
#include "libft/includes/libft.h"
#include "ft_ls.h"

static void		ft_recurse_a(t_data *db, t_opt *opt, char *argv)
{
	t_data	*tmp;
	char	*new_path;

	tmp = db;
	while (tmp)
	{
		if ((*tmp).permissions[0] == 'd' && ft_strcmp((*tmp).path, ".") != 0
				&& ft_strcmp((*tmp).path, "..") != 0)
		{
			new_path = ft_strjoin(argv, (*tmp).path);
			ft_printf("\n%s:\n", new_path);
			ft_process_options(opt, new_path);
		}
		tmp = (*tmp).next;
	}
}

static void		ft_recurse_normal(t_data *db, t_opt *opt, char *argv)
{
	t_data	*tmp;
	char	*new_path;

	tmp = db;
	while (tmp)
	{
		if ((*tmp).permissions[0] == 'd' && (*tmp).path[0] != '.')
		{
			new_path = ft_strjoin(argv, (*tmp).path);
			ft_printf("\n%s:\n", new_path);
			ft_process_options(opt, new_path);
		}
		tmp = (*tmp).next;
	}
}

void			ft_recurse(t_data *db, t_opt *opt, char *argv)
{
	int		len;
	char	allo;
	char	*tmp;

	allo = 0;
	len = ft_strlen(argv);
	if (argv[len - 1] != '/')
	{
		tmp = argv;
		argv = ft_strjoin(argv, "/");
		free(tmp);
		allo = 1;
	}
	if ((*opt).opt_a != 0)
		ft_recurse_a(db, opt, argv);
	else
		ft_recurse_normal(db, opt, argv);
	if (allo == 1)
		free(argv);
}
