/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_db.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 15:21:40 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 16:43:54 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft/includes/libft.h"
#include "ft_ls.h"

static int		ft_lst_split_db(t_data *lst, t_data **head, t_data **mid)
{
	t_data	*tortus;
	t_data	*hare;

	if (lst == NULL || (*lst).next == NULL)
	{
		*head = lst;
		*mid = NULL;
		return (0);
	}
	tortus = lst;
	hare = (*lst).next;
	while (hare)
	{
		hare = (*hare).next;
		if (hare)
		{
			hare = (*hare).next;
			tortus = (*tortus).next;
		}
	}
	*head = lst;
	*mid = (*tortus).next;
	(*tortus).next = NULL;
	return (0);
}

int				ft_compare(t_data *first, t_data *second)
{
	if ((*first).m_time > (*second).m_time)
		return (1);
	else if ((*first).m_time == (*second).m_time)
	{
		if ((*first).m_time_nano > (*second).m_time_nano)
			return (1);
		else if ((*first).m_time_nano == (*second).m_time_nano)
			return (ft_strcmp((*first).path, (*second).path) * -1);
	}
	return (-1);
}

int				ft_compare2(t_data *first, t_data *second)
{
	return (ft_strcmp((*first).path, (*second).path));
}

static t_data	*ft_combine_db(t_data *first, t_data *second,
		int (*f)(t_data *, t_data *), char sign)
{
	t_data	*ret;

	ret = NULL;
	if (first == NULL)
		return (second);
	else if (second == NULL)
		return (first);
	if (f(first, second) * sign <= 0)
	{
		ret = first;
		(*ret).next = ft_combine_db((*first).next, second, f, sign);
	}
	else
	{
		ret = second;
		(*ret).next = ft_combine_db(first, (*second).next, f, sign);
	}
	return (ret);
}

int				ft_merge_sort_db(t_data **lst,
		int (*f) (t_data *, t_data *), char sign)
{
	t_data	*head;
	t_data	*first;
	t_data	*second;

	head = *lst;
	if (head == NULL || (*head).next == NULL)
		return (0);
	ft_lst_split_db(head, &first, &second);
	ft_merge_sort_db(&first, f, sign);
	ft_merge_sort_db(&second, f, sign);
	*lst = ft_combine_db(first, second, f, sign);
	return (0);
}
