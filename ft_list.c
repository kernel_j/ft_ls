/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 16:19:06 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 17:28:25 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "ft_ls.h"

void	ft_add_node(t_data **head, t_data *new)
{
	t_data	*tmp;

	if (head != NULL)
	{
		if (*head == NULL && new != NULL)
			*head = new;
		else if (*head != NULL && new != NULL)
		{
			tmp = *head;
			*head = new;
			(*new).next = tmp;
		}
	}
}

t_data	*ft_new_node(void)
{
	t_data	*head;

	head = (t_data *)malloc(sizeof(*head));
	if (head)
	{
		ft_bzero(head, sizeof(*head));
		return (head);
	}
	exit(1);
}
