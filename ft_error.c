/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 10:40:17 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 16:36:04 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft/includes/ft_printf.h"

void	ft_opt_error(char invalid)
{
	ft_printf("ft_ls: illegal option -- %c\n", invalid);
	ft_printf("usage: ft_ls [-Ralrt] [file ...]\n");
	exit(1);
}

void	ft_empty_arg(void)
{
	ft_printf("ft_ls: fts_open: No such file or directory\n");
	exit(1);
}

int		ft_lstat_error(void)
{
	perror("lstat");
	exit(1);
}

int		ft_stat_error(void)
{
	perror("stat");
	exit(1);
}

void	ft_readlink_error(char *error)
{
	perror(error);
	exit(1);
}
