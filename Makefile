# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/20 16:18:45 by jwong             #+#    #+#              #
#    Updated: 2016/05/17 20:01:29 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= ft_ls

SRC		= main.c			\
			ft_check_file.c	\
			ft_count_order.c\
			ft_dir_processing.c\
		  ft_error.c		\
			ft_error_check.c\
			ft_file_processing.c\
			ft_formats.c	\
			ft_get_file_type.c \
			ft_get_info.c				\
			ft_get_timestamp.c	\
			ft_links.c			\
			ft_list.c				\
		  ft_ls_cleanup.c	\
			ft_options.c		\
			ft_permissions.c	\
			ft_print.c			\
			ft_recurse.c\
		  ft_sort_db.c		\
		  ft_sort_error.c

CC		= clang
CFLAGS	= -Wall -Werror -Wextra
OBJ		= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -lft

%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all
