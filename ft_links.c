/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_links.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 17:58:15 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 18:05:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <unistd.h>
#include "libft/includes/libft.h"
#include "ft_ls.h"

void	ft_get_link(t_data *node, char *name, char *argv)
{
	char	buff[1025];
	char	*link;
	char	*tmp;
	ssize_t	ret;

	tmp = ft_strjoin(argv, "/");
	link = ft_strjoin(tmp, name);
	free(tmp);
	ret = readlink(link, buff, 1024);
	if (ret == -1)
		ft_readlink_error(strerror(errno));
	buff[ret] = '\0';
	tmp = ft_strjoin(name, " -> ");
	(*node).path = ft_strjoin(tmp, buff);
	free(tmp);
	free(link);
}

int		ft_link_test(t_data **db, char *argv)
{
	t_data		*node;
	struct stat	sb;
	char		buff[1025];
	char		*tmp;
	ssize_t		ret;

	if (lstat(argv, &sb) == -1)
		ft_lstat_error();
	if ((sb.st_mode & S_IFMT) == S_IFLNK)
	{
		node = ft_new_node();
		ft_get_info(node, &sb);
		ret = readlink(argv, buff, 1024);
		if (ret == -1)
			ft_readlink_error(strerror(errno));
		buff[ret] = '\0';
		tmp = ft_strjoin(argv, " -> ");
		(*node).path = ft_strjoin(tmp, buff);
		free(tmp);
		ft_add_node(db, node);
		return (0);
	}
	return (-1);
}

int		ft_is_link(char *argv)
{
	struct stat	sb;

	if (lstat(argv, &sb) == -1)
		ft_lstat_error();
	if ((sb.st_mode & S_IFMT) == S_IFLNK)
		return (0);
	return (-1);
}
