/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file_processing.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 19:36:48 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 19:43:09 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include "libft/includes/libft.h"

static void		ft_add_info(t_data **db, struct stat *sb, char *argv)
{
	t_data	*node;

	node = ft_new_node();
	ft_get_info(node, sb);
	(*node).path = ft_strdup(argv);
	ft_add_node(db, node);
}

int				ft_build_file_db(t_data **db, char *argv, char l)
{
	struct stat		sb;
	struct stat		sblink;
	unsigned int	mode;
	int				ret;

	if (lstat(argv, &sb) == -1)
		return (1);
	if ((mode = (sb.st_mode & S_IFMT)) != S_IFDIR)
	{
		if (mode == S_IFLNK)
		{
			if ((ret = stat(argv, &sblink)) != -1)
			{
				if ((sblink.st_mode & S_IFMT) == S_IFDIR && l == 0)
					return (0);
				if (l == 1)
					return (ft_link_test(db, argv));
			}
			else if (ret == -1 && l == 1)
				return (ft_link_test(db, argv));
		}
		ft_add_info(db, &sb, argv);
	}
	return (0);
}

int				ft_process_file_options(t_data **db, t_opt *opt, char *argv)
{
	char	l;

	l = 0;
	if ((*opt).opt_l != 0)
		l = 1;
	ft_build_file_db(db, argv, l);
	return (0);
}

int				ft_file_processing(t_data *arg, t_opt *opt)
{
	t_data	*db;
	t_data	*tmp;

	db = NULL;
	tmp = arg;
	while (tmp)
	{
		if (ft_process_file_options(&db, opt, (*tmp).path) == 1)
			return (1);
		(*arg).used = 1;
		tmp = (*tmp).next;
	}
	if ((*opt).opt_r != 0 && (*opt).opt_t != 0)
		ft_merge_sort_db(&db, ft_compare, 1);
	else if ((*opt).opt_r == 0 && (*opt).opt_t != 0)
		ft_merge_sort_db(&db, ft_compare, -1);
	else if ((*opt).opt_r != 0)
		ft_merge_sort_db(&db, ft_compare2, -1);
	else
		ft_merge_sort_db(&db, ft_compare2, 1);
	if (ft_print_file_db(db, opt) == 0)
		(*opt).was_error = 0;
	ft_clean_lst(&db);
	return (0);
}
