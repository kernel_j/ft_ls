/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_error.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 15:20:36 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 16:45:17 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft/includes/libft.h"
#include "ft_ls.h"

static int			ft_lst_split(t_data *lst, t_data **head, t_data **mid)
{
	t_data	*tortus;
	t_data	*hare;

	if (lst == NULL || (*lst).next == NULL)
	{
		*head = lst;
		*mid = NULL;
		return (0);
	}
	tortus = lst;
	hare = (*lst).next;
	while (hare)
	{
		hare = (*hare).next;
		if (hare)
		{
			hare = (*hare).next;
			tortus = (*tortus).next;
		}
	}
	*head = lst;
	*mid = (*tortus).next;
	(*tortus).next = NULL;
	return (0);
}

static t_data		*ft_combine(t_data *first, t_data *second)
{
	t_data	*ret;

	ret = NULL;
	if (first == NULL)
		return (second);
	else if (second == NULL)
		return (first);
	if (ft_strcmp((*first).error, (*second).error) <= 0)
	{
		ret = first;
		(*ret).next = ft_combine((*first).next, second);
	}
	else
	{
		ret = second;
		(*ret).next = ft_combine(first, (*second).next);
	}
	return (ret);
}

int					ft_merge_sort_error(t_data **lst)
{
	t_data	*head;
	t_data	*first;
	t_data	*second;

	head = *lst;
	if (head == NULL || (*head).next == NULL)
		return (0);
	ft_lst_split(head, &first, &second);
	ft_merge_sort_error(&first);
	ft_merge_sort_error(&second);
	*lst = ft_combine(first, second);
	return (0);
}
