/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_file_type.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 17:27:25 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 17:27:42 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int	ft_get_file_type(struct stat *sb, char *p, unsigned int i)
{
	unsigned int	mode;

	if ((mode = (*sb).st_mode & S_IFMT) == S_IFREG)
		p[i] = '-';
	else if (mode == S_IFDIR)
		p[i] = 'd';
	else if (mode == S_IFLNK)
		p[i] = 'l';
	else if (mode == S_IFCHR)
		p[i] = 'c';
	else if (mode == S_IFSOCK)
		p[i] = 's';
	else if (mode == S_IFIFO)
		p[i] = 'p';
	else if (mode == S_IFBLK)
		p[i] = 'b';
	i++;
	return (i);
}
