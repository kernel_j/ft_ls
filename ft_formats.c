/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_formats.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 18:39:23 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 18:40:05 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "ft_ls.h"

int		ft_show_total(t_data *db, t_opt *opt)
{
	t_data	*tmp;
	int		i;

	if ((*opt).opt_a != 0)
		return (0);
	tmp = db;
	i = 0;
	while (tmp)
	{
		if ((*tmp).path[0] != '.' && (*tmp).permissions[0] != 'l')
			i++;
		tmp = (*tmp).next;
	}
	if (i > 0)
		return (0);
	return (1);
}

char	*ft_path(char *argv, char *d_name)
{
	char	*tmp;
	char	*path;

	path = ft_strjoin(argv, "/");
	tmp = path;
	path = ft_strjoin(path, d_name);
	free(tmp);
	return (path);
}
