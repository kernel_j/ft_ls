/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_cleanup.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/04 13:42:04 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 17:28:43 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_cleanup_error(t_data **err)
{
	t_data	*next;

	while (*err)
	{
		next = (**err).next;
		free((**err).error);
		free(*err);
		*err = next;
	}
}

void	ft_clean_lst(t_data **db)
{
	t_data	*tmp;
	t_data	*tmp2;

	tmp = *db;
	while (tmp)
	{
		if ((*tmp).permissions)
			free((*tmp).permissions);
		if ((*tmp).owner)
			free((*tmp).owner);
		if ((*tmp).group)
			free((*tmp).group);
		if ((*tmp).timestamp)
			free((*tmp).timestamp);
		if ((*tmp).path)
			free((*tmp).path);
		if ((*tmp).error)
			free((*tmp).error);
		tmp2 = tmp;
		tmp = (*tmp).next;
		free(tmp2);
		tmp2 = NULL;
	}
}
