/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 11:14:23 by jwong             #+#    #+#             */
/*   Updated: 2016/05/25 13:11:48 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <dirent.h>
# include <sys/stat.h>
# include <stdlib.h>
# include <time.h>

typedef struct	s_data
{
	struct s_data	*next;
	char			*permissions;
	char			*owner;
	char			*group;
	char			*timestamp;
	char			*path;
	char			*error;
	char			used;
	size_t			num_links;
	size_t			file_size;
	size_t			major;
	size_t			minor;
	size_t			blocks;
	size_t			m_time;
	size_t			m_time_nano;
}				t_data;

typedef	struct	s_opt
{
	char	opt_a		: 1;
	char	opt_l		: 1;
	char	opt_r		: 1;
	char	opt_t		: 1;
	char	opt_br		: 1;
	char	opt_one		: 1;
	char	print_path	: 1;
	char	new_arg		: 1;
	char	invalid;
	char	was_error;
	char	printed;
}				t_opt;

/*
**	ft_count_order.c
*/
void			ft_order_arg(t_data **arg, int i, int argc, char **argv);
int				ft_count_files(int argc, char **argv);
size_t			ft_get_total_blocks(t_data *db, t_opt *opt);

/*
**	ft_dir_processing.c
*/
int				ft_build_dir_db(t_data **db, char *argv, char l);
int				ft_process_options(t_opt *opt, char *argv);
int				ft_dir_processing(t_data *arg, t_opt *opt, int i);

/*
**	ft_ls_cleanup.c
*/
void			ft_cleanup_error(t_data **err);
void			ft_clean_lst(t_data **db);

/*
**	ft_error.c
*/
void			ft_opt_error(char invalid);
void			ft_empty_arg(void);
int				ft_stat_error(void);
int				ft_lstat_error(void);
void			ft_readlink_error(char *error);

/*
**	ft_error_check.c
*/
void			ft_build_error_branch(t_data **err, char *argv, char *info);
int				ft_error_check(t_data *arg, int i, int argc);
/*
**	ft_file_processing.c
*/
int				ft_build_file_db(t_data **db, char *argv, char l);
int				ft_process_file_options(t_data **db, t_opt *opt, char *argv);
int				ft_file_processing(t_data *arg, t_opt *opt);

/*
**	ft_formats.c
*/
int				ft_show_total(t_data *db, t_opt *opt);
char			*ft_path(char *argv, char *d_name);

/*
**	ft_get_info.c
*/
void			ft_get_info(t_data *node, struct stat *sb);
void			ft_get_names(t_data *node, struct stat *sb);

/*
**	ft_links.c
*/
void			ft_get_link(t_data *node, char *name, char *argv);
int				ft_link_test(t_data **db, char *argv);
int				ft_is_link(char *argv);

/*
**	ft_list.c
*/
void			ft_add_node(t_data **head, t_data *new);
t_data			*ft_new_node(void);

/*
**	ft_print.c
*/
void			ft_print_error(t_data *db);
void			ft_print_opt_l(t_data *tmp);
void			ft_print_path(t_data *tmp, t_opt *opt, char *argv);
void			ft_print_db(t_data *db, t_opt *opt, char *argv);
int				ft_print_file_db(t_data *db, t_opt *opt);

/*
**	ft_sort_db.c
*/
int				ft_merge_sort_error(t_data **lst);
int				ft_merge_sort_db(t_data **lst,
				int (*f) (t_data *, t_data *), char sign);
int				ft_compare(t_data *first, t_data *second);
int				ft_compare2(t_data *first, t_data *second);

int				ft_check_file(char *argv);
size_t			ft_get_total_blocks(t_data *db, t_opt *opt);
void			ft_get_timestamp(t_data *node, time_t times);
void			ft_get_permissions(struct stat *sb, char *p, unsigned int i);
int				ft_get_file_type(struct stat *sb, char *p, unsigned int i);
int				ft_parse_options(t_opt *opt, int argc, char **argv);
void			ft_recurse(t_data *db, t_opt *opt, char *argv);
#endif
