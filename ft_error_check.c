/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_check.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 20:25:43 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 20:26:04 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include "libft/includes/ft_printf.h"
#include "libft/includes/libft.h"
#include "ft_ls.h"

void		ft_build_error_branch(t_data **err, char *argv, char *info)
{
	t_data	*new;
	size_t	total;

	total = ft_strlen(argv) + ft_strlen(info) + 11;
	new = ft_new_node();
	ft_bzero(new, sizeof(new));
	(*new).error = (char *)malloc(sizeof(char) * total);
	if ((*new).error)
		ft_sprintf((*new).error, "ft_ls: %s: %s\n", argv, info);
	ft_add_node(err, new);
}

static int	ft_error_lst(t_data *err)
{
	ft_merge_sort_error(&err);
	ft_print_error(err);
	ft_cleanup_error(&err);
	return (1);
}

int			ft_error_check(t_data *arg, int i, int argc)
{
	t_data	*err;
	t_data	*tmp;
	DIR		*dirp;

	err = NULL;
	tmp = arg;
	while (i++ < argc && tmp)
	{
		if ((*tmp).path[0] == '\0')
			ft_empty_arg();
		if ((dirp = opendir((*tmp).path)) == NULL)
		{
			if (ft_check_file((*tmp).path) == 1)
			{
				ft_build_error_branch(&err, (*tmp).path, strerror(errno));
				(*tmp).used = 1;
			}
		}
		else
			closedir(dirp);
		tmp = (*tmp).next;
	}
	if (err != NULL)
		return (ft_error_lst(err));
	return (0);
}
