/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_permissions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 17:24:59 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 17:27:16 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int			ft_user_permissions(struct stat *sb,
		char *p, unsigned int i)
{
	if ((*sb).st_mode & S_IRUSR)
		p[i++] = 'r';
	else
		p[i++] = '-';
	if ((*sb).st_mode & S_IWUSR)
		p[i++] = 'w';
	else
		p[i++] = '-';
	if ((*sb).st_mode & S_IXUSR)
		p[i] = 'x';
	else
		p[i] = '-';
	if (p[i] == 'x' && ((*sb).st_mode & S_ISUID))
		p[i] = 's';
	else if (p[i] == '-' && ((*sb).st_mode & S_ISUID))
		p[i] = 'S';
	i++;
	return (i);
}

static unsigned int	ft_group_permissions(struct stat *sb,
		char *p, unsigned int i)
{
	if ((*sb).st_mode & S_IRGRP)
		p[i++] = 'r';
	else
		p[i++] = '-';
	if ((*sb).st_mode & S_IWGRP)
		p[i++] = 'w';
	else
		p[i++] = '-';
	if ((*sb).st_mode & S_IXGRP)
		p[i] = 'x';
	else
		p[i] = '-';
	if (p[i] == 'x' && ((*sb).st_mode & S_ISGID))
		p[i] = 's';
	else if (p[i] == '-' && ((*sb).st_mode & S_ISGID))
		p[i] = 'S';
	i++;
	return (i);
}

static void			ft_other_permissions(struct stat *sb,
		char *p, unsigned int i)
{
	if ((*sb).st_mode & S_IROTH)
		p[i++] = 'r';
	else
		p[i++] = '-';
	if ((*sb).st_mode & S_IWOTH)
		p[i++] = 'w';
	else
		p[i++] = '-';
	if ((*sb).st_mode & S_IXOTH)
		p[i] = 'x';
	else
		p[i] = '-';
	if (p[i] == 'x' && ((*sb).st_mode & S_ISVTX))
		p[i] = 't';
	else if (p[i] == '-' && ((*sb).st_mode & S_ISVTX))
		p[i] = 'T';
}

void				ft_get_permissions(struct stat *sb,
		char *p, unsigned int i)
{
	i = ft_user_permissions(sb, p, i);
	i = ft_group_permissions(sb, p, i);
	ft_other_permissions(sb, p, i);
}
