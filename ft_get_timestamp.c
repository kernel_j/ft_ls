/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_timestamp.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 17:46:32 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 17:46:46 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "ft_ls.h"

static void	ft_parse_timestamp(t_data *node, char *stamp)
{
	char	*tmp;
	char	*tmp2;

	tmp = ft_strsub(stamp, 4, 6);
	tmp2 = ft_strsub(stamp, 19, 5);
	(*node).timestamp = ft_strjoin(tmp, tmp2);
	free(tmp);
	free(tmp2);
}

void		ft_get_timestamp(t_data *node, time_t times)
{
	time_t	current;
	char	*stamp;

	stamp = ft_strdup(ctime(&times));
	current = time(NULL);
	if (current == -1)
		exit(1);
	if (current > times)
	{
		if ((current - times) > 15292800)
			ft_parse_timestamp(node, stamp);
		else
			(*node).timestamp = ft_strsub(stamp, 4, 12);
	}
	else
	{
		if ((times - current) > 15292800)
			ft_parse_timestamp(node, stamp);
		else
			(*node).timestamp = ft_strsub(stamp, 4, 12);
	}
	free(stamp);
}
