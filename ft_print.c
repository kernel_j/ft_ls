/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 18:25:42 by jwong             #+#    #+#             */
/*   Updated: 2016/05/25 13:11:20 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/ft_printf.h"
#include "libft/includes/libft.h"
#include "ft_ls.h"

void	ft_print_error(t_data *db)
{
	t_data *tmp;

	tmp = db;
	while (tmp)
	{
		ft_printf("%s", (*tmp).error);
		tmp = (*tmp).next;
	}
}

void	ft_print_opt_l(t_data *tmp)
{
	if ((*tmp).permissions[0] != 'c' && (*tmp).permissions[0] != 'b')
		ft_printf("%s %5zu %s %s %6zu %s %s\n",\
				(*tmp).permissions, (*tmp).num_links,\
				(*tmp).owner, (*tmp).group, (*tmp).file_size,\
				(*tmp).timestamp, (*tmp).path);
	else
		ft_printf("%s %5zu %s %s %5zu, %zu %s %s\n",\
				(*tmp).permissions, (*tmp).num_links, (*tmp).owner,\
				(*tmp).group, (*tmp).major, (*tmp).minor, (*tmp).timestamp,\
				(*tmp).path);
}

void	ft_print_path(t_data *tmp, t_opt *opt, char *argv)
{
	if ((tmp && (*opt).print_path != 0 && (*opt).opt_br == 0)
			|| ((*opt).opt_br != 0 && (*opt).new_arg != 0))
	{
		if (!(*opt).was_error && (*opt).printed)
			ft_printf("\n");
		ft_printf("%s:\n", argv);
		(*opt).new_arg = 0;
		(*opt).was_error = 0;
		(*opt).printed = 1;
	}
}

void	ft_print_db(t_data *db, t_opt *opt, char *argv)
{
	t_data	*tmp;

	tmp = db;
	ft_print_path(tmp, opt, argv);
	if ((*opt).opt_l != 0 && ft_show_total(db, opt) == 0)
		ft_printf("total %zu\n", ft_get_total_blocks(db, opt));
	while (tmp)
	{
		if ((*opt).opt_a != 0 && (*opt).opt_l != 0)
			ft_print_opt_l(tmp);
		else if ((*opt).opt_a == 0 && (*opt).opt_l != 0)
		{
			if ((*tmp).path[0] != '.')
				ft_print_opt_l(tmp);
		}
		else if ((*opt).opt_a != 0 && (*opt).opt_l == 0)
			ft_printf("%s\n", (*tmp).path);
		else
		{
			if ((*tmp).path[0] != '.')
				ft_printf("%s\n", (*tmp).path);
		}
		tmp = (*tmp).next;
	}
}

int		ft_print_file_db(t_data *db, t_opt *opt)
{
	t_data	*tmp;

	if (db == NULL)
		return (1);
	tmp = db;
	while (tmp)
	{
		if ((*opt).opt_l != 0)
			ft_print_opt_l(tmp);
		else
			ft_printf("%s\n", (*tmp).path);
		tmp = (*tmp).next;
	}
	(*opt).printed = 1;
	return (0);
}
