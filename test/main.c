/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 16:20:49 by jwong             #+#    #+#             */
/*   Updated: 2016/04/29 18:15:44 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>

int		main(int argc, char **argv)
{
	int				i;
	DIR				*dirp; 
	struct dirent	*dp;
	struct stat		st;
	
	if (argc < 0)
		return (0);
	i = 1;
	while (i <= argc)
	{
		dirp = opendir(argv[i]);
		if (dirp == NULL)
			return (0);
		while ((dp = readdir(dirp)) != NULL)
		{
			stat((*dp).d_name, &st);
			if ((st.st_mode & S_IFMT) == S_IFREG)
				printf("file\n");
			write(1, (*dp).d_name, strlen((*dp).d_name));
			write(1, "\n", 1);
		}
		closedir(dirp);
		i++;
	}
	return (0);
}
