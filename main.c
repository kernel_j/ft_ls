/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 11:26:10 by jwong             #+#    #+#             */
/*   Updated: 2016/05/25 15:52:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "ft_ls.h"

int		ft_ls(t_opt *opt, int argc, char **argv, int i)
{
	t_data	*arg;
	int		ret;

	arg = NULL;
	ret = 0;
	ft_order_arg(&arg, i, argc, argv);
	if ((ret = ft_error_check(arg, i, argc)))
		(*opt).was_error = 1;
	if (ft_file_processing(arg, opt) == 1)
		ret = 1;
	if (ft_dir_processing(arg, opt, i) == 1)
		ret = 1;
	if ((*opt).opt_br == 0)
		ft_clean_lst(&arg);
	return (ret);
}

int		main(int argc, char **argv)
{
	t_opt	opt;
	int		i;

	i = 1;
	ft_bzero(&opt, sizeof(opt));
	if (argc < 1 || argc > 4096)
		return (1);
	i = ft_parse_options(&opt, argc, argv);
	if (ft_count_files(argc, argv) == 0)
	{
		ft_process_options(&opt, "./");
		return (0);
	}
	else if (ft_count_files(argc, argv) > 1)
		opt.print_path = 1;
	return (ft_ls(&opt, argc, argv, i));
}
