/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_info.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 17:54:20 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 17:54:38 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <grp.h>
#include <pwd.h>
#include "libft/includes/libft.h"
#include "ft_ls.h"


void	ft_get_names(t_data *node, struct stat *sb)
{
	struct passwd	*pwd;
	struct group	*gr;

	if ((pwd = getpwuid((*sb).st_uid)))
		(*node).owner = ft_strdup((*pwd).pw_name);
	if ((gr = getgrgid((*sb).st_gid)))
		(*node).group = ft_strdup((*gr).gr_name);
}

void	ft_get_info(t_data *node, struct stat *sb)
{
	char			permission[11];
	unsigned int	i;

	ft_bzero(permission, 11);
	i = 0;
	i = ft_get_file_type(sb, permission, i);
	ft_get_permissions(sb, permission, i);
	(*node).permissions = ft_strdup(permission);
	(*node).num_links = (size_t)(*sb).st_nlink;
	if (permission[0] == 'c' || permission[0] == 'b')
	{
		(*node).minor = (int)((*sb).st_rdev & 0xff);
		(*node).major = (int)(((*sb).st_rdev >> 24) & 0x7f);
	}
	else
		(*node).file_size = (size_t)(*sb).st_size;
	(*node).blocks = (size_t)(*sb).st_blocks;
	ft_get_names(node, sb);
	(*node).m_time = (*sb).st_mtime;
#ifdef __APPLE__
	(*node).m_time_nano = (*sb).st_mtimespec.tv_nsec;
#elif __linux__
	(*node).m_time_nano = (*sb).st_mtim.tv_nsec;
#endif
	ft_get_timestamp(node, (*sb).st_mtime);
}
