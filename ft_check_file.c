/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 13:09:45 by jwong             #+#    #+#             */
/*   Updated: 2016/05/25 13:40:01 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "ft_ls.h"

static int	ft_name_check(char *file, char *path)
{
	struct dirent	*dp;
	DIR				*dirp;

	if ((dirp = opendir(path)) != NULL)
	{
		while ((dp = readdir(dirp)) != NULL)
		{
			if (ft_strcmp(file, (*dp).d_name) == 0)
			{
				closedir(dirp);
				free(file);
				free(path);
				return (0);
			}
		}
		closedir(dirp);
	}
	free(file);
	free(path);
	return (1);
}

int			ft_check_file(char *argv)
{
	int				len;
	int				i;
	char			*path;
	char			*file;

	len = ft_strlen(argv);
	i = len;
	while (--i >= 0)
	{
		if (argv[i] == '/')
		{
			path = ft_strsub(argv, 0, len - (len - i - 1));
			file = ft_strsub(argv, i + 1, len - i);
			break ;
		}
	}
	if (i < 0)
	{
		path = ft_strdup("./");
		file = ft_strdup(argv);
	}
	return (ft_name_check(file, path));
}
