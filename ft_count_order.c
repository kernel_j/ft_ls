/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_order.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/24 18:44:09 by jwong             #+#    #+#             */
/*   Updated: 2016/05/24 18:45:08 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/includes/libft.h"
#include "ft_ls.h"

size_t	ft_get_total_blocks(t_data *db, t_opt *opt)
{
	t_data	*tmp;
	size_t	total;

	total = 0;
	tmp = db;
	if ((*opt).opt_a != 0)
	{
		while (tmp)
		{
			total += (*tmp).blocks;
			tmp = (*tmp).next;
		}
	}
	else
	{
		while (tmp)
		{
			if ((*tmp).path[0] != '.')
				total += (*tmp).blocks;
			tmp = (*tmp).next;
		}
	}
	return (total);
}

int		ft_count_files(int argc, char **argv)
{
	int	i;
	int	total;

	i = 1;
	total = 0;
	while (i < argc && argv[i][0] == '-')
	{
		if (argv[i][1] == '\0')
			total++;
		i++;
	}
	while (i < argc && argv[i][0] != '-')
	{
		i++;
		total++;
	}
	return (total);
}

void	ft_order_arg(t_data **arg, int i, int argc, char **argv)
{
	t_data	*node;

	while (i < argc)
	{
		if (argv[i])
		{
			node = ft_new_node();
			(*node).path = ft_strdup(argv[i]);
			ft_add_node(arg, node);
		}
		i++;
	}
	ft_merge_sort_db(arg, ft_compare, -1);
}
